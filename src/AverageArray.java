
public class AverageArray {

	public static void main(String[] args) {
		int avg;
		int sum = 0;
		int[] arr = { 4, 10, 15, 7 };
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		avg = sum / arr.length;
		System.out.println(avg);
	}

}
