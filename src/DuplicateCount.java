
public class DuplicateCount {

	public static void main(String[] args) {
		int[] arr = new int[] { 10, 19, 1, 10, 0, 1, 3, 10, 19 };
		int duplicate;
		System.out.print("This number are duplicate: ");
		for (int i = 0; i < arr.length; i++) {
			duplicate = arr[i];
			for (int j = i+1; j < arr.length; j++) {
				if (arr[j] == duplicate && j != i && duplicate==arr[i]) {
					System.out.print(duplicate+", ");
					break;
				}
			}
		}
	}

}
