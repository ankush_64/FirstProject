import java.util.Arrays;

public class RemoveDuplicate {
	public static void main(String[] args) {
		int[] arrayone = { 1, 2, 5, 5, 8, 9, 7, 10 };
		// int[] arraytwo = { 1, 0, 6, 15, 6, 4, 7, 0 };
		int no_unique_elements = arrayone.length;
		for (int i = 0; i < no_unique_elements; i++) {
			for (int j = i + 1; j < arrayone.length; j++) {
				if (arrayone[i] == (arrayone[j])) {

					arrayone[j] = arrayone[no_unique_elements - 1];

					no_unique_elements--;

					j--;

				}
			}
		}
		int[] array1 = Arrays.copyOf(arrayone, no_unique_elements);

		// Printing arrayWithoutDuplicates

		System.out.println();

		System.out.println("Array with unique values : ");

		for (int i = 0; i < array1.length; i++) {
			System.out.print(array1[i] + "\t");
		}

		System.out.println();

		System.out.println("---------------------------");
	}

}
