
public class ReverseArray {

	public static void main(String[] args) {
		int [] arr= new int []{10,20,90,40,25};
		for (int i = 0; i < arr.length/2; i++) {
	int temp=arr[i];
	arr[i]=arr[arr.length-i-1];
	arr[arr.length-i-1]=temp;
		}
		System.out.print("After reverse array is: ");
for (int i = 0; i < arr.length; i++) {
	System.out.print(arr[i]+", ");
}
	}

}
