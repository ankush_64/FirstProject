
public class SumArray {

	public static void main(String[] args) {
		int sum = 0;
		int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		System.out.println("Sum of an array values are: " + sum);
	}

}
